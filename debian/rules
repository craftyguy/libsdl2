#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

confflags = --disable-rpath --enable-sdl-dlopen \
            --disable-nas --disable-esd --disable-arts \
            --disable-alsa-shared --disable-pulseaudio-shared \
            --enable-ibus \
            --disable-x11-shared --disable-video-directfb \
            --enable-video-opengles \
            --disable-video-opengles1 \
            --enable-video-wayland --disable-wayland-shared \
            --enable-video-kmsdrm --disable-kmsdrm-shared

# disable autoheader (invoked automatically by autoreconf), necessary in order
# to use debhelper compat level v10 without overriding dh-autoreconf calls
export AUTOHEADER := /bin/true


ifeq ($(DEB_HOST_ARCH_CPU),powerpc)
  confflags += --disable-altivec
endif

ifeq ($(DEB_HOST_ARCH_CPU),ppc64el)
  confflags += --disable-altivec
endif

# disable Wayland and Vulkan on non-Linux, they do not support other kernels at the moment
ifeq (hurd,$(findstring hurd,$(DEB_HOST_ARCH_CPU)))
  confflags += --disable-video-vulkan
  confflags += --disable-video-wayland
endif
ifeq (kfreefsd,$(findstring kfreebsd,$(DEB_HOST_ARCH_CPU)))
  confflags += --disable-video-vulkan
  confflags += --disable-video-wayland
endif

# disable OpenGLES on Hurd, it does not support it at the moment
ifeq (hurd,$(findstring hurd,$(DEB_HOST_ARCH_CPU)))
  confflags += --disable--video-opengles
endif


%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- $(confflags)

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))

override_dh_auto_build-indep:
	GZIP="-9n" tar czf debian/examples.tar.gz test --owner=0 --group=0 --mode=go=rX,u+rw,a-s --clamp-mtime --mtime="@$(SOURCE_DATE_EPOCH)" --sort=name
	doxygen docs/doxyfile
	# useless files
	find output -name "*.md5" -delete
	find output -type d -empty -delete

	find output -name "jquery.js" -delete
	dh_link -plibsdl2-doc usr/share/javascript/jquery/jquery.js usr/share/doc/libsdl2-doc/html/jquery.js

# Force examples to be installed in libsdl2-doc, it does not happen with compat
# level v11 despite having the file debian/libsdl2-doc.examples (it gets
# installed as part of libsdl2-dev instead)
override_dh_installexamples-indep:
	dh_installexamples -i --doc-main-package=libsdl2-doc

endif # !nocheck

override_dh_auto_build-arch:
	dh_auto_build -- V=1

override_dh_auto_clean-indep:
	dh_auto_clean
	rm -f debian/examples.tar.gz
	rm -rf output

override_dh_install:
	mkdir -p debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/SDL2
	mv debian/tmp/usr/include/SDL2/SDL_config.h debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/SDL2/_real_SDL_config.h
	ln -s ../../SDL2/SDL_platform.h debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/SDL2/
	ln -s ../../SDL2/begin_code.h debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/SDL2/
	ln -s ../../SDL2/close_code.h debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/SDL2/
	dh_install

override_dh_missing:
	dh_missing --fail-missing

override_dh_link:
	# to address lintian warning
	# W: libsdl2-2.0-0: dev-pkg-without-shlib-symlink usr/lib/x86_64-linux-gnu/libSDL2-2.0.so.0.0.0 usr/lib/x86_64-linux-gnu/libSDL2-2.0.so
	dh_link -plibsdl2-dev usr/lib/$(DEB_HOST_MULTIARCH)/libSDL2-2.0.so.0 usr/lib/$(DEB_HOST_MULTIARCH)/libSDL2-2.0.so
	dh_link --remaining-packages

override_dh_installchangelogs:
	dh_installchangelogs -- WhatsNew.txt
